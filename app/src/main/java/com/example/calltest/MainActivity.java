package com.example.calltest;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.net.URISyntaxException;

public class MainActivity extends AppCompatActivity {

    private Button btnSend;
    private EditText KeyEditText;
    private EditText Param1EditText;
    private EditText Param2EditText;
    private TextView tv;
    private JSONObject jsonObject;
    final private static String key = "key";
    final private static String params = "params";

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("http://13.209.208.217:80");
        } catch (URISyntaxException e) {
            System.out.println("socket init failed " + e);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSocket.on("response", onResponseMsg);
        mSocket.connect();

        KeyEditText = (EditText) findViewById(R.id.editTextKey);
        Param1EditText= (EditText) findViewById(R.id.editTextParam1);
        Param2EditText = (EditText) findViewById(R.id.editTextParam2);
        btnSend = (Button) findViewById(R.id.buttonSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                send();
            }
        });
        tv = (TextView) findViewById(R.id.textView);
    }
    private void send() {
        jsonObject = new JSONObject();
        JSONArray arr = new JSONArray();
        try {
            jsonObject.put(key, KeyEditText.getText().toString());
            arr.put(Param1EditText.getText().toString());
            arr.put(Param2EditText.getText().toString());
            jsonObject.put(params, arr);
        } catch (org.json.JSONException e) {
            System.out.println("write json err "+e);
        }
        tv.append("[REQ]["+KeyEditText.getText().toString()+"]["+arr.toString()+"]\n");
        mSocket.emit("request",jsonObject.toString());
    }

    private Emitter.Listener onResponseMsg = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String keys;
                    JSONArray paramss;
                    JSONObject object;
                    try {
                        keys = data.getString(key);
                        paramss = data.getJSONArray(params);
                        tv.append("[RES]["+keys+"]["+paramss.toString()+"]\n");
                        System.out.println("get response from socket server key : "+keys+"values : "+paramss.toString());
                        if ( paramss.length()> 0) {
                            object = paramss.getJSONObject(0);
                            System.out.println(object.toString());
                        }
                    } catch ( JSONException e ) {
                        System.out.println(e);
                    }
                }
            });
        }
    };
    private Activity getActivity () {
        return this;
    }
}
